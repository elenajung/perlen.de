<?php


if(isset($_GET['action'])){
    $action = $_GET['action'];
} else{
    exit;
}

if($action == 'doWpLogin'){
    
    $publicKey = getPublicKey();
    
    $username = decrypt(base64_decode($_GET["encryptUsername"]), $publicKey);
    $password = decrypt(base64_decode($_GET["encryptPassword"]), $publicKey);
    $wordPressUrl = decrypt(base64_decode($_GET["project_url"]), $publicKey);
    $fallbackUrl = decrypt(base64_decode($_GET["fallback_url"]), $publicKey);

    //Einbinden von Wordpress darf erst geschehen, sobald die Daten decrypted wurden!
    require( dirname( __FILE__ ) . '/wp-blog-header.php' );
    
    wpSignon($username, $password, $wordPressUrl, $fallbackUrl);
    
}else if($action == 'createProjectBackup'){
    
    if(isset($_GET['fallbackurl'])) {
        $fallbackUrl = $_GET['fallbackurl'];
    }
    
    if(isset($_GET['project_id'])) {
        $project_id = $_GET['project_id'];
    }
    ini_set('memory_limit', '365M');
    set_time_limit(200);
    
    
    $filelist = getDirectoryList( dirname( __FILE__ ) );
    
    $backup = createBackupZip($filelist);
    
    //Falls die Erstellung der Zip Datei erfolgreich war wird der IndexController im MissionCOntrol aufgerufen, um die Datei in den Globalen Backup Ordner zuladen.
    if($backup){
        header('Location: '. $fallbackUrl.'/index/downloadBackup?project_id='.$project_id);
        exit;
    } else {
        header('Location: '. $fallbackUrl.'?errorMessage=2');
        exit;
    }
}

function getPublicKey(){
    $file = 'publickey.php';
    if ( ! file_exists($file)) {
        return FALSE;
    }
    
    if (function_exists('file_get_contents')) {
        return file_get_contents($file);
    }

    if ( ! $fp = @fopen($file, FOPEN_READ)) {
        return FALSE;
    }

    flock($fp, LOCK_SH);

    $data = '';
    if (filesize($file) > 0) {
        $data =& fread($fp, filesize($file));
    }

    flock($fp, LOCK_UN);
    fclose($fp);
    
    return $data;
}

//Entschluesselt die Daten
function decrypt($decryptData, $publicKey) {
    openssl_public_decrypt($decryptData, $decrypted, $publicKey);
    
    return $decrypted;
}

//Fuehrt den Wordpress Login Versuch durch
function wpSignon($username, $password, $wordPressUrl, $fallbackUrl) {
    
    $creds = array();
    $creds['user_login'] = $username;
    $creds['user_password'] = $password;
    $creds['remember'] = true;
    $user = wp_signon( $creds, false );
    
    wp_set_current_user($user->ID, $user->user_login);
    
    if(is_user_logged_in()){
        
        $secure_cookie = is_ssl();
        
        $secure_cookie = apply_filters('secure_signon_cookie', $secure_cookie, $creds);
        add_filter('authenticate', 'wp_authenticate_cookie', 30, 3);
        
        $user = wp_authenticate($creds['user_login'], $creds['user_password']);
        wp_set_auth_cookie($user->ID, $creds["remember"], $secure_cookie);

        wp_redirect($wordPressUrl."/wp-admin/index.php");
        exit;
    } else {
        header('Location: '. $fallbackUrl.'?errorMessage=1');
        exit;
    }
}

function createBackupZip($filelist){
    $zip = new ZipArchive;
    $zip_filename = $_SERVER['DOCUMENT_ROOT'].'backup/'.date('d_m_Y').'.zip';
    if ( ( $retval = $zip->open( $zip_filename, ZIPARCHIVE::CREATE ) ) === true ) {

        foreach($filelist as $file){
            if(!empty($file)){
                $zip->addFile( $file );
            }
        }
        $result = $zip->close();
        return $result;
    } else{
        return false;
    }
}


function getDirectoryList($dir) {
    $pathlist = array( );

    if ($dfp = @opendir($dir)) {
        while (($entry = readdir($dfp)) !== false) {
            if ($entry != "." && $entry != "..") { // catches dot dirs and hidden files
                $path = realpath( "$dir/$entry" );
                if ( is_file($path)) {
		    $pathlist[] = $path;
		}
                else if (is_dir($path)) {
                    $pathlist_sub = getDirectoryList($path);
		    for( $t = 0; $t < count( $pathlist_sub ); $t++ ) {
			$pathlist[] = $pathlist_sub[$t];
		    }
                }
            }
        }
        @closedir($dfp);

    }

    return $pathlist;
}