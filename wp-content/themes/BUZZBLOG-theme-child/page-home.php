<?php 
/**
* Template Name: HOMEPAGE
*/
get_header(); ?>

<div class="content-holder clearfix">
	<div class="container">
		<div class="row">			
                    <div class="span12">			
                        <div class="row">                   
                            <div class="span12" id="title-header">					
                                <div class="page-header">                       
                                    <?php get_template_part("static/static-customtitle"); ?>                   
                                </div>					
                            </div>               
                        </div>	
                        
                      <?php if ( is_active_sidebar( 'hs_main_slideshow' ) ) : ?>
				
                        <div class="row">			
                            <div class="span12">				
                                <?php dynamic_sidebar("hs_main_slideshow"); ?>				
                            </div>				
                        </div>
								
                       <?php endif; ?>
				
                        <div class="row">

                            <div class="span8 <?php if (of_get_option('blog_sidebar_pos')==''){echo 'right';}else{echo of_get_option('blog_sidebar_pos'); } ?>" id="content">
                               <div class="menu-wrap clearfix">
                                    <?php wp_nav_menu( array(
                                        'container'		=> false,
                                        'menu_id'       => 12,
                                        'depth'         => 0,
                                        'menu_class' => 'home-menu-grid',
                                        'items_wrap'    => '<ul data-breakpoint="800" id="%1$s" class="%2$s">%3$s</ul>',
                                        'theme_location'=> 'pearl_menu',
                                        'walker' => new pic_walker()
                                    )); ?>                        
                               </div>
                                 <!-- Featured Post Bereich Anfang -->
                                    <div class="featured_post">
                                        
                                        <?php $my_query = new WP_Query('category_name=Featured&showposts=1');
                                        while ($my_query->have_posts()) : $my_query->the_post();
                                        $do_not_duplicate = $post->ID; ?>
                                        
                                            <div class="type-page clearfix">
                                                <span class="handwrite">Featured</span>
                                                <?php the_post_thumbnail('full'); ?> 
                                                <h1><?php the_title(); ?></h1>
                                                 <?php the_excerpt(); ?> 
                                                <?php get_template_part( 'includes/post-formats/share-buttons' ); ?>
                                            </div>
                                        
                                        <?php endwhile;
                                              wp_reset_query();
                                         ?>
                                    </div>

                                <!-- Featured Post Bereich Ende -->
                                <div class="menu-wrap clearfix">
                                    <?php wp_nav_menu( array(
                                        'container'		=> false,
                                        'menu_id'       => 11,
                                        'depth'         => 0,
                                        'menu_class' => 'home-menu-grid',
                                        'items_wrap'    => '<ul data-breakpoint="800" id="%1$s" class="%2$s">%3$s</ul>',
                                        'theme_location'=> 'schmuck_menu',
                                        'walker' => new pic_walker()
                                    )); ?>
                                </div>
                                  <!-- Content Bereich Anfang -->
                                <?php // if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

                                    <?php // if($post->post_content=="") : else : ?>
                                <div id="post-separate" <?php post_class('page clearfix'); ?>>
                                        <h1><?php the_title(); ?></h1>        
                                        <?php the_content(); ?>
                                        <?php get_template_part( 'includes/post-formats/share-buttons' ); ?>
                                </div><!--#post-->
                                        <?php // endif; ?>
                                <?php // endwhile;?>
                                <!-- Content Bereich Ende -->
				
                            </div>
									
                            <div class="span4 sidebar" id="sidebar">						
                                <?php dynamic_sidebar("hs_main_sidebar"); ?>					
                            </div>									
                        </div>			
                    </div>		
                </div>	
        </div>
</div>
<footer class="footer">
<?php get_template_part('wrapper/wrapper-footer'); ?>
</footer>
<?php get_footer(); ?>