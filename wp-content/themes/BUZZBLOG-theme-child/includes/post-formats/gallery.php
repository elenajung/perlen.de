<article id="post-<?php the_ID(); ?>" <?php post_class('post__holder'); ?>>
<?php formaticons(); ?>
	<header class="post-header">	
		<?php if(!is_singular()) : ?>
			<h2 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php echo theme_locals('permalink_to');?> <?php the_title(); ?>"><?php the_title(); ?></a></h2>
		<?php else :?>
			<h2 class="post-title"><?php the_title(); ?></h2>
		<?php endif; ?>
	</header>
<?php $post_meta = of_get_option('post_meta');
     if ($post_meta=='true' || $post_meta=='') {
	 get_template_part('includes/post-formats/post-meta'); 
	 } ?>
	
	<?php 
	$hercules_gallery_type = get_post_meta(get_the_ID(), 'tz_gallery_format', true);
	$hercules_targetheight = get_post_meta(get_the_ID(), 'tz_gallery_targetheight', true);
	$hercules_random = hs_gener_random(10);
	?>

	<div class="post-thumb clearfix">
	<?php if ($hercules_gallery_type=='slideshow') { 
	global $hercules_add_owl;
    $hercules_add_owl = true; ?>
	<script type="text/javascript">
		jQuery(window).load(function() {
jQuery("#owl-demo_<?php echo $hercules_random ?>").owlCarousel({
    autoPlay : 5000,
    stopOnHover : true,
    navigation:false,
	//items : 2,
    paginationSpeed : 1000,
    goToFirstSpeed : 2000,
    singleItem : true,
    autoHeight : true,
    transitionStyle:"fadeUp",
	afterInit: function(){
    }
								
			});
		});
	</script>
		<!-- Slider -->
			<div id="owl-demo_<?php echo $hercules_random ?>" class="owl-carousel" style="margin: 0px 0px 1.5em;">
								
					<?php 

						$hercules_attachments = get_children(array('post_parent' => get_the_ID(), 'post_type' => 'attachment', 'post_mime_type' => 'image' ));
					
						if ($hercules_attachments) :					
						foreach ($hercules_attachments as $attachment) :
							
							$url = $attachment->ID;
						$blog_thumb_width = of_get_option('blog_thumb_width');
			$blog_thumb_height = of_get_option('blog_thumb_height');
			$image = vt_resize( $url,'' , $blog_thumb_width, $blog_thumb_height, true, 90 );
					?>
					
					<div><img src="<?php echo $image['url']; ?>" width="<?php echo $image['width']; ?>" height="<?php echo $image['height']; ?>" alt="<?php echo apply_filters('the_title', $attachment->post_title); ?>"/></div>
					
					<?php 
						endforeach;
						endif;
					?>
			</div>
			<!-- /Slider -->		
		<?php } ?>
		
		<!-- Grid -->
		<?php if ($hercules_gallery_type=='grid') {
		global $hercules_add_collageplus;
    $hercules_add_collageplus = true;
		?>
		<script type="text/javascript">
	jQuery(document).ready(function () {
			jQuery(".justifiedgall_<?php echo $hercules_random ?>").justifiedGallery({
				rowHeight: <?php echo $hercules_targetheight; ?>,
				//fixedHeight: true,
				margins: 10
			}); });

    </script>
					<div class="zoom-gallery justifiedgall_<?php echo $hercules_random ?>" style="margin: 0px 0px 1.5em;">
					<div class="spinner"><span></span><span></span><span></span></div>			
					<?php 

						$hercules_attachments = get_children(array('post_parent' => get_the_ID(), 'post_type' => 'attachment', 'post_mime_type' => 'image' ));
					
						if ($hercules_attachments) :					
						foreach ($hercules_attachments as $attachment) :
							$attachment_url = wp_get_attachment_image_src( $attachment->ID, 'gallery-large' );
							
			$caption = apply_filters('the_title', $attachment->post_excerpt);
					?>
					<a class="zoomer" title="<?php echo apply_filters('the_title', $attachment->post_excerpt); ?>" data-source="<?php echo $attachment_url[0]; ?>" href="<?php echo $attachment_url[0]; ?>"><?php echo wp_get_attachment_image($attachment->ID, 'gallery-large'); ?></a>
					
					<?php 
						endforeach;
						endif;
					?>
			</div>

		<?php } ?>
		<!-- /Grid -->
	<div class="row-fluid">
	<div class="span12">
		<?php 
	$full_content = of_get_option('full_content');
	if(!is_singular() && $full_content!='true') : ?>				
	<!-- Post Content -->
	<div class="post_content">
		<?php $post_excerpt = of_get_option('post_excerpt');
$blog_excerpt = of_get_option('blog_excerpt_count');		?>
		<?php if ($post_excerpt=='true') { ?>		
			<div class="excerpt">			
			<?php 
				$content = get_the_content();
			if (has_excerpt()) {
				the_excerpt();
			} else {
				echo limit_text($content,$blog_excerpt);
			} ?>			
			</div>
		<?php } else if ($post_excerpt=='') {
				the_content('<div class="readmore-button">'.theme_locals("continue_reading").'</div>');
		        wp_link_pages('before=<div class="pagelink">&after=</div>'); ?>
		<div class="clear"></div>
		<?php } ?>
				<?php $readmore_button = of_get_option('readmore_button');
if ($readmore_button=='yes') { ?>
				<div class="readmore-button">
		<a href="<?php the_permalink() ?>" class=""><?php echo theme_locals("continue_reading"); ?></a>
</div>	
		<div class="clear"></div>
		<?php } ?>
	</div>
				
	<?php else :?>	
	<!-- Post Content -->
	<div class="post_content">	
		<?php the_content('<div class="readmore-button">'.theme_locals("continue_reading").'</div>'); ?>
		<?php wp_link_pages('before=<div class="pagelink">&after=</div>'); ?>
		<div class="clear"></div>
	</div>
	<!-- //Post Content -->	
	<?php endif; ?>
	</div>	</div>	
	
</div>
<?php get_template_part( 'includes/post-formats/share-buttons' ); ?>
</article><!--//.post__holder-->