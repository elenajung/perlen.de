<?php /* Loop Name: Loop page */ ?>  
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<?php if($post->post_content=="") :

else : ?>
    <div id="post-separate" <?php post_class('page clearfix'); ?>>
        
        <?php $post_meta = of_get_option('post_meta');
     if ($post_meta=='true' || $post_meta=='') {
	 get_template_part('includes/post-formats/post-meta'); 
        } ?>
        <h1><?php the_title(); ?></h1>        
        <?php the_content(); ?>
        <?php get_template_part( 'includes/post-formats/share-buttons' ); ?>
        <?php wp_link_pages('before=<div class="pagination">&after=</div>'); ?><!--.pagination-->
    </div><!--#post-->
    
        <?php comments_template('', true); ?>
	<?php endif; ?>
<?php endwhile; ?>
