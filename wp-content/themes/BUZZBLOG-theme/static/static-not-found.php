<?php /* Static Name: Not found */ ?>
<div>
    <?php echo '<div class="h1">' . __('Sorry!', HS_CURRENT_THEME) . '</div>'; ?>
    <?php echo '<div class="h2">' . __('Page Not Found', HS_CURRENT_THEME) . '</div>'; ?>
</div>

<?php echo '<div class="h4">' . __('The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.', HS_CURRENT_THEME) . '</div>'; ?>
<?php echo '<p>' . __('Please try using our search box below to look for information on the internet.', HS_CURRENT_THEME) . '</p>'; ?>

<?php get_search_form(); /* outputs the default Wordpress search form */ ?>