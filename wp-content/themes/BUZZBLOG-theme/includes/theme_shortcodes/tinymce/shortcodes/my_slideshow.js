frameworkShortcodeAtts={
	attributes:[
			{
				label:"How many slides to show?",
				id:"num",
				help:"This is how many slides will be displayed."
			},
			{
				label:"Which category to pull from?",
				id:"custom_category",
				help:"Enter the slug of the category you'd like to pull slides from. Leave blank if you'd like to pull from all categories."
			},
			{
				label:"Pause between slides",
				id:"autoplay",
				help:"Pause between slides in milliseconds. Example: 5000 is a 5 seconds."
			},
			{
				label:"Height of the slideshow",
				id:"slideheight",
				help:"Enter height of the slideshow in px unit."
			},
			{
				label:"Do you want to open link in a new window?",
				id:"blankwindow",
				controlType:"select-control", 
				selectValues:['true', 'false'],
				defaultValue: 'true', 
				defaultText: 'true',
				help:"Choose true to open link in a new window."
			},
			{
				label:"Pagination",
				id:"pagination",
				controlType:"select-control", 
				selectValues:['true', 'false'],
				defaultValue: 'true', 
				defaultText: 'true',
				help:"Enable pagination."
			},
			{
				label:"Effect",
				id:"effect",
				controlType:"select-control", 
				selectValues:['fade', 'backSlide', 'goDown', 'fadeUp'],
				defaultValue: 'fade', 
				defaultText: 'fade',
				help:"Choose the transition effect."
			},
			{
				label:"Custom class",
				id:"custom_class",
				help:"Use this field if you want to use a custom class."
			}
	],
	defaultContent:"",
	shortcode:"slideshow"
};