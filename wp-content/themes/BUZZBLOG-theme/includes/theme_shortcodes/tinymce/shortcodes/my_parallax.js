frameworkShortcodeAtts={
	attributes:[
			{
				label:"Image",
				id:"photourl",
				help:"Enter the image URL"
			},
			{
				label:"Vertical offset",
				id:"verticaloffset",
				help:"Enter the vertical offset value"
			},
			{
                label:"Title",
                id:"title",
                help:"Enter title."
            },
            {
                label:"Text",
                id:"text",
				controlType:"textarea-control", 
                help:"Enter text."
            },
            {
                label:"Top and bottom text margin",
                id:"textmargin",
                help:"Enter the top and bottom margin value (Do not enter the px unit.)"
            },
            {
                label:"Overlay color",
                id:"overlaycolor",
                help:"Enter the overlay color"
            },
            {
                label:"Overlay opacity",
                id:"overlayopacity",
                help:"Enter the Overlay opacity value"
            }
			
			
			
	],
	defaultContent:"",
	shortcode:"parallax"
};