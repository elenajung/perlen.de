<?php
/*
Template Name: Standard mit Sidebar-Left
*/
?>

<?php get_header(); ?> 
 
 <div class="inner-pad"></div>
 

<div id="main">		
	<div class="columns"> 

    <div class="left_col" id="sidebar">
    	<ul>
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(2) ) : ?>							
                 Add Sidebar Widgets
            <?php endif; ?>  			 
        </ul>
    </div>			
    <div class="narrowcolumn singlepage leftsidebar">
     <?php if (have_posts()) : ?>
     <?php while (have_posts()) : the_post(); ?>							
			<div class="post">
            	
                <div class="title">
				<h1><?php the_title(); ?></h1>
                <!--<small><?php _e('Posted by','nattywp'); ?> <?php theme_get_profile() ?> <?php _e('in','nattywp'); ?> <?php the_category(' | ');?> <?php edit_post_link(__('Edit','nattywp'), ' | ', ''); ?></small> -->
                </div>                
				<div class="entry">
                     <?php t_show_video($post->ID); ?>
                     <?php the_content(); ?>    
                    <div class="clear"></div>
                </div>              
                
				<p class="postmetadata">	               
                <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>        			
                <span class="category"><?php the_tags('', ', ', ''); ?></span>	
				</p>
        

                
			</div>	
			
	<?php endwhile; ?>	
	<?php else : ?>
		<div class="post">
		<h2><?php _e('Not Found','nattywp'); ?></h2>
            <div class="entry"><p><?php _e('Sorry, but you are looking for something that isn\'t here.','nattywp'); ?></p>
            <?php get_search_form(); ?>
            </div>
        </div>
    <?php endif; ?>				
	
        </div> <!-- END Narrowcolumn -->

<div class="clear"></div>
<?php get_footer(); ?> 










