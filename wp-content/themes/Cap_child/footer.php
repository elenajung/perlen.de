<div id="footer">
<?php if ( is_active_sidebar( 'footernavi' ) ) : ?>
<div id="footernavi"><ul>
<?php if ( !dynamic_sidebar( 'footernavi' ) ) : ?>
<?php endif; ?>
</ul></div>
<?php endif; ?>
<div class="clear"></div>			
</div>
    </div> <!-- END Columns --> 
    
   
</div><!-- END main -->
</div>  
<div class="clear"></div>

       
<?php
	$t_analytics = t_get_option( "t_analytics" );
	if( $t_analytics != "" ) { 
?>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>

<script type="text/javascript">
var pageTracker = _gat._getTracker("<?php echo $t_analytics; ?>");
pageTracker._initData();
pageTracker._trackPageview();
</script>
<?php } ?>

<?php $t_tracking = t_get_option( "t_tracking" );
if ($t_tracking != ""){
echo stripslashes(stripslashes($t_tracking));
	}
?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-39967096-14', 'muschelkernperlen.de');
  ga('send', 'pageview');

</script>

<?php wp_footer(); ?>       
</body>
</html>
  
