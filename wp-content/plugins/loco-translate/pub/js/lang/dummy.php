<?php
/**
 * Here for source code extraction until xgettext is applied to js files too
 */
_x('OK','Message label');
_x('Warning','Message label');
_x('Error','Message label');
__('Unknown error');
__('PO file saved');
__('and MO file compiled');
__('Merged from %s');
__('Merged from source code');
__('Already up to date with %s');
__('Already up to date with source code');
_n('1 new string added','%s new strings added', 2 );
_n('1 obsolete string removed','%s obsolete strings removed', 2 );
__('Your changes will be lost if you continue without saving');


/**
 * Legacy translations, removed from front end but don't want to lose in case reinstated 
 */
__('Translation');